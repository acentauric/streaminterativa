<?php
//DEFINIÇÃO DO TÍTULO DA PÁGINA
$titulo_da_pagina = "Stream Interativa - Viewer - Perfil - Editar";

//INICIA SESSÕES NO SISTEMA
session_start();

//VERIFICA SE O USUÁRIO JÁ ESTÁ AUTENTICADO
if (!isset($_SESSION["id_usuario"]))
{
	//RETORNA PARA A TELA DE ERRO COM O CÓDIGO DO ERRO
	header('Location: home_entrar.php?erro=autenticacao');
}
//SE O USUÁRIO JÁ ESTÁ AUTENTICADO
else
{
	//INCLUI AS VARIÁVEIS DE ACESSO AO BANCO DE DADOS
	include ('include/acesso_bd.php');
	
	//CAPTURA O ID DO USUÁRIO ENVIADO PELA URL
	$id_usuario = $_SESSION["id_usuario"];
	
	//VERIFICA SE NÃO FOI ENVIADA A AÇÃO PARA A PÁGINA
	if (!isset($_GET["acao"]))
	{
		//QUERY PARA CONSULTAR OS DADOS CADASTRADOS DO USUÁRIO
		$query_select = "
		SELECT 
			email, senha, nm_jogo, nm_usuario_jogo
		FROM 
			stin_usuarios U
			LEFT JOIN stin_jogos J ON (U.id_usuario = J.id_usuario) 			
		WHERE 
			U.id_usuario = '".$id_usuario."'";
			
		//EXECUTA A CONSULTA NO BANCO DE DADOS
		$select = mysql_query($query_select,$connect);

		//ARMAZENA OS DADOS DA CONSULTA EM UM ARRAY
		$array = mysql_fetch_array($select);

		//CAPTURA EM VARIÁVEIS OS DADOS DO BANCO DE DADOS
		$email 				= $array['email'];
		$senha 				= $array['senha'];
		$nm_jogo 			= $array['nm_jogo'];
		$nm_usuario_jogo 	= $array['nm_usuario_jogo'];
				
		//REALIZA UM TRATAMENTO PARA HABILITAR A OPÇÃO SELECIONADA CORRETAMENTE NO COMBO JOGO
		if ($nm_jogo == ""){$jogo_op1_selected = "selected";}
		elseif ($nm_jogo == "League of Legends"){$jogo_op2_selected = "selected";}
	}
	//VERIFICA SE FOI ENVIADA AÇÃO PARA A PÁGINA
	elseif (isset($_GET["acao"]) && $_GET["acao"] == 'atualizar')
	{
		//CAPTURA OS DADOS EVIADOS PELO FORMULÁRIO
		$email = (isset($_POST["email"]) && $_POST["email"] != '') ? addslashes(trim($_POST["email"])) : $email = "";
		$senha = (isset($_POST["senha"]) && $_POST["senha"] != '') ? addslashes(trim($_POST["senha"])) : $senha = "";
		$repetir_senha = (isset($_POST["repetir_senha"]) && $_POST["repetir_senha"] != '') ? addslashes(trim($_POST["repetir_senha"])) : $repetir_senha = "";
		$nm_jogo = (isset($_POST["nm_jogo"]) && $_POST["nm_jogo"] != '') ? addslashes(trim($_POST["nm_jogo"])) : $nm_jogo = "";
		$nm_usuario_jogo = (isset($_POST["nm_usuario_jogo"]) && $_POST["nm_usuario_jogo"] != '') ? addslashes(trim($_POST["nm_usuario_jogo"])) : $nm_usuario_jogo = "";
		$nm_canal = (isset($_POST["nm_canal"]) && $_POST["nm_canal"] != '') ? addslashes(trim($_POST["nm_canal"])) : $nm_canal = "";
		$nm_link_canal = (isset($_POST["nm_link_canal"]) && $_POST["nm_link_canal"] != '') ? addslashes(trim($_POST["nm_link_canal"])) : $nm_link_canal = "";
		
		//CRIPTOGRAFA A SENHA PARA UTILIZAÇÃO ADEQUADA
		$senha = MD5($senha);
		
		//VERIFICA SE OS DADOS OBRIGATÓRIOS NÃO FORAM INFORMADOS
		if ($email == "" || $senha == "")
		{
			//RETORNA PARA A TELA DE ERRO COM O CÓDIGO RELATIVO
			$erro = "dados_vazios";
		}
		//SE OS DADOS OBRIGATÓRIOS FORAM INFORMADOS
		else
		{
			//QUERY PARA VERIFICAR SE O EMAIL INFORMADO JÁ EXISTE NO BANCO DE DADOS
			$query_select = "SELECT email FROM stin_usuarios WHERE email = '".$email."' AND id_usuário <> ".$id_usuario."";

			//EXECUTA A QUERY NO BANCO DE DADOS
			$select = mysql_query($query_select, $connect);

			//VERIFICA SE O EMAIL INFORMADO JÁ EXISTE NO BANCO DE DADOS
			if (mysql_num_rows($select))
			{
				//RETORNA PARA A TELA DE ERRO COM O CÓDIGO RESPECTIVO
				$erro = "dados_existentes";
			}
			//O EMAIL INFORMADO NÃO EXISTE NO BANCO DE DADOS
			else
			{
				//VERIFICA SE A NOVA SENHA FOI INFORMADA
				if ($senha != "d41d8cd98f00b204e9800998ecf8427e")
				{
					//CRIA A QUERY PARA REALIZAR A ATUALIZAÇÃO DO USUÁRIO NO SISTEMA (DADOS BÁSICOS)
					$query_update = "
					UPDATE stin_usuarios SET 
					senha =  '".$senha."', email =  '".$email."' 
					WHERE id_usuario = ".$id_usuario."";
					
					//EXECUTA A QUERY NO BANCO DE DADOS
					$update = mysql_query($query_update, $connect);

					//VERIFICA SE A ATUALIZAÇÃO FOI REALIZADA COM SUCESSO
					if(!$update)
					{						
						//RETORNA PARA A TELA DE ERRO COM O CÓDIGO RESPECTIVO
						$erro = "atualizar_email_senha";
					}
				}
				//SE A NOVA SENHA NÃO FOI INFORMADA
				else
				{
					//CRIA A QUERY PARA REALIZAR A ATUALIZAÇÃO DO USUÁRIO NO SISTEMA (DADOS BÁSICOS)
					$query_update = "
					UPDATE stin_usuarios SET 
					email =  '".$email."' 
					WHERE id_usuario = ".$id_usuario."";		
					
					//EXECUTA A QUERY NO BANCO DE DADOS
					$update = mysql_query($query_update, $connect);

					//VERIFICA SE A ATUALIZAÇÃO FOI REALIZADA COM SUCESSO
					if(!$update)
					{						
						//RETORNA PARA A TELA DE ERRO COM O CÓDIGO RESPECTIVO
						$erro = "atualizar_email";
					}
				}
				//CRIA A QUERY PARA REALIZAR A ATUALIZAÇÃO DO USUÁRIO NO SISTEMA (DADOS DO JOGO)
				$query_update = "
				UPDATE stin_jogos SET 
				nm_jogo =  '".$nm_jogo."', nm_usuario_jogo = '".$nm_usuario_jogo."' 
				WHERE id_usuario = ".$id_usuario."";
				
				//EXECUTA A QUERY NO BANCO DE DADOS
				$update = mysql_query($query_update, $connect);
				
				//VERIFICA SE A ATUALIZAÇÃO FOI REALIZADA COM SUCESSO
				if(!$update)
				{	
					//RETORNA PARA A TELA DE ERRO COM O CÓDIGO RESPECTIVO
					$erro = "atualizar_jogo";
				}				
			}			
		}
		
		//VERIFICA SE NÃO HOUVE ENNHUM ERRO NA ATUALIZAÇÃO
		if ($erro == "")
		{
			//ENVIA PARA A TELA DE INICIO DE UTILIZACAO
			header('Location: viewer_perfil.php?mensagem=dados_atualizados');
		}
		
	}
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include ('layout_head.php'); ?>
</head>
<body id="page-top">
	<?php include ('layout_menu.php'); ?>
	<section class="bg-primary">
        <div class="container">
			<div class="row">
				<div class="col-md-12">
					<h1 class="section-heading text-center">ATUALIZE OS SEUS DADOS</h1>
					<p class="text-center">Faça a atualização cuidadosa dos seus dados para evitar erros.<br/>Dessa forma poderemos organizar a plataforma mais adequadadamente para te auxiliar.</p>
					<hr class="light">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="index.php">Home</a></li>
						<li class="breadcrumb-item"><a href="viewer_inicio.php">Viewer</a></li>
						<li class="breadcrumb-item"><a href="viewer_perfil.php">Perfil</a></li>
						<li class="breadcrumb-item active">Editar</li>
					</ol>
				</div>
			</div>
			<form id="form" action="viewer_perfil_editar.php?acao=atualizar" method="post">
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label for="email">Email</label>
							<input type="email" class="form-control" id="email" name="email"  placeholder="Informe um Email válido" required value="<?=$email?>">
						</div>
					</div>	
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label for="senha">Senha</label>
							<input type="password" class="form-control" id="senha" name="senha" placeholder="Informe uma Nova Senha para acesso">
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label for="senha">Repetir Senha</label>
							<input type="password" class="form-control" id="repetir_senha" name="repetir_senha" placeholder="Repita a Nova Senha informada">
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">							
						<div class="form-group">
							<label for="email">Jogo</label>
							<select class="form-control" name="nm_jogo">
								<option value="" <?=$jogo_op1_selected?>>( Selecione )</option>
								<option value="League of Legends" <?=$jogo_op2_selected?>>League of Legends</option>
							</select>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label for="nm_usuario_jogo">Usuário do Jogo</label>
							<input type="text" class="form-control" id="nm_usuario_jogo" name="nm_usuario_jogo"  placeholder="Informe o seu Usuário do Jogo" value="<?=$nm_usuario_jogo?>">
						</div>
					</div>
				</div>	
				<div class="row">
					<div class="col-md-12">
						<button type="submit" class="btn btn-default">Enviar</button>
					</div>
				</div>
			</form>			
        </div>
    </section>

    <section id="contact">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 text-center">
                    <h2 class="section-heading">Dúvidas, Sugestões ou Reclamações?</h2>
                    <hr class="primary">
                    <p>Nós valorizamos muito a sua opnião e será por meio dela que construiremos uma plataforma cada vez melhor!</p>
                </div>
                <div class="col-lg-4 col-lg-offset-2 text-center">
                    <i class="fa fa-whatsapp fa-3x sr-contact"></i>
                    <p>(71) 99369-9538</p>
                </div>
                <div class="col-lg-4 text-center">
                    <i class="fa fa-envelope-o fa-3x sr-contact"></i>
                    <p><a href="mailto:contato@alphacentauri.com.br">contato@alphacentauri.com.br</a></p>
                </div>
            </div>
        </div>
    </section>

    <!-- jQuery -->
    <script src="vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
    <script src="vendor/scrollreveal/scrollreveal.min.js"></script>
    <script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>

    <!-- Theme JavaScript -->
    <script src="js/creative.min.js"></script>

</body>

</html>
