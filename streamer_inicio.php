<?php
//DEFINIÇÃO DO TÍTULO DA PÁGINA
$titulo_da_pagina = "Stream Interativa - Streamer - Início";

//INICIA SESSÕES NO SISTEMA
session_start();
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include ('layout_head.php'); ?>
</head>
<body id="page-top">
    <?php include ('layout_menu.php'); ?>
    <section class="bg-primary">
        <div class="container">
			<div class="row">
				<div class="col-md-12">
					<h1 class="section-heading text-center">BEM VINDO AO SISTEMA</h1>		
					<p class="text-center">
						Agora você poderá utilizar as funcionalidades existentes em nossa plataforma.<br/>
						Escolha uma das opções disponíveis abaixo para continuar.
					</p>
					<hr class="light">
					<div class="col-md-6 text-center">					
						<i class="fa fa-4x fa-id-card"></i>
						<h3>Perfil</h3>
						<p class="text-faded">Este acesso permitirá que você confira as informações cadastradas no seu Perfil conforme base de dados coletada automaticamente.</p>
						<a href="streamer_perfil.php" class="page-scroll btn btn-default btn-xl sr-button">Acessar</a>
					</div>
					<div class="col-md-6 text-center">					
						<i class="fa fa-4x fa-tasks"></i>
						<h3>Painel</h3>
						<p class="text-faded">Este acesso permitirá que você inicie o processo de interação com as pessoas que te acompanham por meio do canal informado.</p>
						<a href="streamer_painel.php" class="page-scroll btn btn-default btn-xl sr-button">Acessar</a>
					</div>
				</div>
			</div>				
        </div>
    </section>
    <?php include ('layout_footer.php'); ?>
	<?php include ('layout_scripts.php'); ?>	
</body>
</html>