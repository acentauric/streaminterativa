<nav id="mainNav" class="navbar navbar-default navbar-fixed-top">
	<div class="container-fluid">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
				<span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
			</button>
			<a class="navbar-brand page-scroll" href="#page-top">STREAM INTERATIVA</a>
		</div>
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav navbar-right">
				
				<?php
				//CRIA UMA VARIÁVEL DE INDICE PARA AUXILIAR A MATRIZ DE  LINKS
				$indice = 0;
				
				//VERIFICA QUAL O TÓTULO DA PÁGINA PARA CRIAR O MENU
				switch ($titulo_da_pagina) 
				{
					case "Stream Interativa - Home":
					
						//DEFINE AS OPÇÕES NO MENU
						$menu[$indice]['link'] = "#funcionamento";
						$menu[$indice++]['texto'] = "FUNCIONAMENTO";
						
						$menu[$indice]['link'] = "#funcionalidades";
						$menu[$indice++]['texto'] = "FUNCIONALIDADES";
						
						$menu[$indice]['link'] = "#footer";
						$menu[$indice++]['texto'] = "CONTATO";
						
						//VERIFICA SE O USUÁRIO AINDA N]AO ESTÁ LOGADO
						if (!isset($_SESSION["id_usuario"]))
						{
							//ADICIONA AS OPÇÕES DE REGISTRO E DE LOGIN NO MENU
							$menu[$indice]['link'] = "home_registrar.php";
							$menu[$indice++]['texto'] = "[ REGISTRAR ]";
							
							$menu[$indice]['link'] = "home_entrar.php";
							$menu[$indice++]['texto'] = "[ ENTRAR ]";
						}
						//VERIFICA SE O USUÁRIO AINDA NÃO ESTÁ LOGADO
						else
						{
							//VERIFICA SE O USUÁRIO É DO TIPO STREAMER
							if (in_array("1", $_SESSION["id_tipo_usuario"]))
							{
								//ADICIONA A OPÇÃO DE LOGOUT NO MENU
								$menu[$indice]['link'] = "streamer_inicio.php";
								$menu[$indice++]['texto'] = "[ INÍCIO ]";
							}
							//VERIFICA SE O USUÁRIO É DO TIPO VIEWER
							elseif (in_array("2", $_SESSION["id_tipo_usuario"]))
							{
								//ADICIONA A OPÇÃO DE LOGOUT NO MENU
								$menu[$indice]['link'] = "viewer_inicio.php";
								$menu[$indice++]['texto'] = "[ INÍCIO ]";
							}
							
							$menu[$indice]['link'] = "home_sair.php";
							$menu[$indice++]['texto'] = "[ SAIR ]";
						}
						
					break;
					case "Stream Interativa - Home - Registrar":
						$menu[$indice]['link'] = "index.php";
						$menu[$indice++]['texto'] = "HOME";
						
						$menu[$indice]['link'] = "#footer";
						$menu[$indice++]['texto'] = "CONTATO";
						
						$menu[$indice]['link'] = "home_entrar.php";
						$menu[$indice++]['texto'] = "[ ENTRAR ]";
					break;
					case "Stream Interativa - Home - Entrar":
						$menu[$indice]['link'] = "index.php";
						$menu[$indice++]['texto'] = "HOME";
						
						$menu[$indice]['link'] = "#footer";
						$menu[$indice++]['texto'] = "CONTATO";
						
						$menu[$indice]['link'] = "home_registrar.php";
						$menu[$indice++]['texto'] = "[ REGISTRAR ]";
					break;
					case "Stream Interativa - Streamer - Início":
						$menu[$indice]['link'] = "index.php";
						$menu[$indice++]['texto'] = "HOME";
						
						$menu[$indice]['link'] = "#footer";
						$menu[$indice++]['texto'] = "CONTATO";
						
						$menu[$indice]['link'] = "home_sair.php";
						$menu[$indice++]['texto'] = "[ SAIR ]";
					break;
					case "Stream Interativa - Streamer - Cadastrar":
						$menu[$indice]['link'] = "index.php";
						$menu[$indice++]['texto'] = "HOME";
						
						$menu[$indice]['link'] = "#footer";
						$menu[$indice++]['texto'] = "CONTATO";
					break;
					case "Stream Interativa - Streamer - Perfil":
						$menu[$indice]['link'] = "index.php";
						$menu[$indice++]['texto'] = "HOME";
						
						$menu[$indice]['link'] = "#footer";
						$menu[$indice++]['texto'] = "CONTATO";
						
						$menu[$indice]['link'] = "home_sair.php";
						$menu[$indice++]['texto'] = "[ SAIR ]";
					break;
					case "Stream Interativa - Streamer - Perfil - Editar":
						$menu[$indice]['link'] = "index.php";
						$menu[$indice++]['texto'] = "HOME";
						
						$menu[$indice]['link'] = "#footer";
						$menu[$indice++]['texto'] = "CONTATO";
						
						$menu[$indice]['link'] = "home_sair.php";
						$menu[$indice++]['texto'] = "[ SAIR ]";
					break;
					case "Stream Interativa - Streamer - Painel":
						$menu[$indice]['link'] = "index.php";
						$menu[$indice++]['texto'] = "HOME";
						
						$menu[$indice]['link'] = "#footer";
						$menu[$indice++]['texto'] = "CONTATO";
						
						$menu[$indice]['link'] = "home_sair.php";
						$menu[$indice++]['texto'] = "[ SAIR ]";
					break;
					case "Stream Interativa - Viewer - Cadastrar":
						$menu[$indice]['link'] = "index.php";
						$menu[$indice++]['texto'] = "HOME";
						
						$menu[$indice]['link'] = "#footer";
						$menu[$indice++]['texto'] = "CONTATO";
					break;
					case "Stream Interativa - Viewer - Perfil":
						$menu[$indice]['link'] = "index.php";
						$menu[$indice++]['texto'] = "HOME";
						
						$menu[$indice]['link'] = "#footer";
						$menu[$indice++]['texto'] = "CONTATO";
						
						$menu[$indice]['link'] = "home_sair.php";
						$menu[$indice++]['texto'] = "[ SAIR ]";
					break;
					case "Stream Interativa - Viewer - Perfil - Editar":
						$menu[$indice]['link'] = "index.php";
						$menu[$indice++]['texto'] = "HOME";
						
						$menu[$indice]['link'] = "#footer";
						$menu[$indice++]['texto'] = "CONTATO";
						
						$menu[$indice]['link'] = "home_sair.php";
						$menu[$indice++]['texto'] = "[ SAIR ]";
					break;
					case "Stream Interativa - Viewer - Painel":
						$menu[$indice]['link'] = "index.php";
						$menu[$indice++]['texto'] = "HOME";
						
						$menu[$indice]['link'] = "#footer";
						$menu[$indice++]['texto'] = "CONTATO";
						
						$menu[$indice]['link'] = "home_sair.php";
						$menu[$indice++]['texto'] = "[ SAIR ]";
					break;
					default:
						$menu[$indice]['link'] = "index.php";
						$menu[$indice++]['texto'] = "HOME";
						
						$menu[$indice]['link'] = "#footer";
						$menu[$indice++]['texto'] = "CONTATO";
					break;						
				}
				
				//PERCORRE O VETOR DE OPÇÕES DO MENU CRIANDO O MESMO
				for ($i = 0; $i < count($menu); $i++) 
				{
					//MONTA A ESTRUTURA HTML DA OPÇÃO NO MEU
					echo '<li>';
					echo '<a class="page-scroll" href="'.$menu[$i]['link'].'">'.$menu[$i]['texto'].'</a>';
					echo '<li>';
				}
				
				?>
				
			</ul>
		</div>		
	</div>	
</nav>