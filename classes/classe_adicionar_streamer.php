<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Stream Interativa - Uma nova forma de Stremar</title>

    <!-- Bootstrap Core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>

    <!-- Plugin CSS -->
    <link href="vendor/magnific-popup/magnific-popup.css" rel="stylesheet">

    <!-- Theme CSS -->
    <link href="css/creative.min.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body id="page-top">

    <nav id="mainNav" class="navbar navbar-default navbar-fixed-top">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand page-scroll" href="#page-top">STREAM INTERATIVA</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a class="page-scroll" href="index.php">Home</a>
                    </li>                    
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>

    <section class="bg-primary">
        <div class="container">
			<div class="row">
				<h1 class="section-heading text-center">INFORME OS SEUS DADOS PARA CADASTRAR-SE</h1>
				<p class="text-center">Faça o cadastro informando o maior número de informações possíveis. Dessa forma poderemos organizar a plataforma de forma mais adequada para te auxiliar devidamente..</p>
				<hr class="light">
				<form action="adicionar_streamer.php">
					<div class="form-group">
						<label for="email">Email</label>
						<input type="email" class="form-control" id="email" name="email" placeholder="Email">
					</div>
					<div class="form-group">
						<label for="senha">Senha</label>
						<input type="password" class="form-control" id="senha" name="senha" placeholder="Senha">
					</div>
					<div class="form-group">
						<label for="senha">Repetir Senha</label>
						<input type="password" class="form-control" id="repetir_senha" name="repetir_senha" placeholder="Senha">
					</div>
					<hr class="light">
					<div class="form-group">
						<label for="email">Jogo</label>
						<select class="form-control" name="jogo">
							<option value="0">( Selecione )</option>
							<option value="1">League of Legends</option>
						</select>
					</div>
					<div class="form-group">
						<label for="email">Usuário do Jogo</label>
						<input type="text" class="form-control" id="usuario_do_jogo" name="usuario_do_jogo" placeholder="Usuário do Jogo">
					</div>
					<hr class="light">
					<div class="form-group">
						<label for="email">Canal</label>
						<select class="form-control" name="canal">
							<option value="0">( Selecione )</option>
							<option value="1">Youtube</option>
						</select>
					</div>
					<div class="form-group">
						<label for="email">Link do Jogo</label>
						<input type="text" class="form-control" id="link_do_canal" name="link_do_canal" placeholder="Link do Canal">
					</div>
					<button type="submit" class="btn btn-default">Enviar</button>
				</form>
			</div>				
        </div>
    </section>

    <section id="contact">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 text-center">
                    <h2 class="section-heading">Dúvidas, Sugestões ou Reclamações?</h2>
                    <hr class="primary">
                    <p>Nós valorizamos muito a sua opnião e será por meio dela que construiremos uma plataforma cada vez melhor!</p>
                </div>
                <div class="col-lg-4 col-lg-offset-2 text-center">
                    <i class="fa fa-whatsapp fa-3x sr-contact"></i>
                    <p>(71) 99369-9538</p>
                </div>
                <div class="col-lg-4 text-center">
                    <i class="fa fa-envelope-o fa-3x sr-contact"></i>
                    <p><a href="mailto:contato@alphacentauri.com.br">contato@alphacentauri.com.br</a></p>
                </div>
            </div>
        </div>
    </section>

    <!-- jQuery -->
    <script src="vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
    <script src="vendor/scrollreveal/scrollreveal.min.js"></script>
    <script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>

    <!-- Theme JavaScript -->
    <script src="js/creative.min.js"></script>

</body>

</html>
