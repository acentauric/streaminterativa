<?php
//DEFINIÇÃO DO TÍTULO DA PÁGINA
$titulo_da_pagina = "Stream Interativa - Home";

//INICIA SESSÕES NO SISTEMA
session_start();
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include ('layout_head.php'); ?>
</head>
<body id="page-top">
	<?php include ('layout_menu.php'); ?>
    <header>
        <div class="header-content">
            <div class="header-content-inner">
                <h1 id="homeHeading">AGORA AS STREAMS PODEM SER AINDA MAIS INTERATIVAS</h1>
                <hr>
                <p>
					Esta é uma plataforma de interação entre Streamers e Viewers.<br/>
					Envie e Receba sugestões durante as Sreams de forma mais organizada.
				</p>
                <a href="#funcionamento" class="btn btn-primary btn-xl page-scroll">Entenda o Funcionamento</a>
            </div>
        </div>
    </header>	
    <section class="bg-primary" id="funcionamento">
        <div class="container">
			<div class="row">
				<h2 class="section-heading text-center">Saiba como a Plataforma funciona</h2>
				<div class="col-md-4 text-center">
					<hr class="light">
					<i class="fa fa-4x fa-desktop"></i>
					<p class="text-faded">Serão disponibilizados Painéis para que Viewers e Streamers possam interagir.</p>				
				</div>
				<div class="col-md-4 text-center">
					<hr class="light">
					<i class="fa fa-4x fa-comments"></i>
					<p class="text-faded">As interações serão baseadas nas mecânicas e sistemas de decisão do jogo transmitido.</p>                    
					<a href="#funcionalidades" class="page-scroll btn btn-default btn-xl sr-button">Conheça as Funcionalidades</a>
				</div>
				<div class="col-md-4 text-center">
					<hr class="light">
					<i class="fa fa-4x fa-gamepad"></i>
					<p class="text-faded">O Streamer receberá sugestões dos Viewers que poderão ser aplicadas durante o jogo.</p>					
				</div>				
			</div>			
        </div>
    </section>
    <section id="funcionalidades">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">Funcionalidades Disponíveis</h2>
                    <hr class="primary">
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-6 text-center">
                    <div class="service-box">
                        <i class="fa fa-4x fa-random text-primary sr-icons"></i>
                        <h3>Sugerir Lane</h3>
                        <p class="text-muted">Sugira a Lane que você deseja que o Streamer jogue.</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 text-center">
                    <div class="service-box">
                        <i class="fa fa-4x fa-users text-primary sr-icons"></i>
                        <h3>Sugerir Champion</h3>
                        <p class="text-muted">Sugira o Champion que você deseja que o Streamer escolha.</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 text-center">
                    <div class="service-box">
                        <i class="fa fa-4x fa-cubes text-primary sr-icons"></i>
                        <h3>Sugerir Build</h3>
                        <p class="text-muted">Sugira a Build que você deseja que o Streamer utilize.</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 text-center">
                    <div class="service-box">
                        <i class="fa fa-4x fa-tags text-primary sr-icons"></i>
                        <h3>Sugerir Targets</h3>
                        <p class="text-muted">Sugira os Targets que você deseja que o Streamer realize.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <aside class="bg-dark" id="iniciar">
        <div class="container text-center">
            <div class="call-to-action">
                <h2>Se sentiu interessado?</h2>
                <a href="home_registrar.php" class="btn btn-default btn-xl sr-button">Começe agora!</a>
            </div>
        </div>
    </aside>
    <?php include ('layout_footer.php'); ?>
	<?php include ('layout_scripts.php'); ?>	
</body>
</html>
