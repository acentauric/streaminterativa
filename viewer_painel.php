<?php
//DEFINIÇÃO DO TÍTULO DA PÁGINA
$titulo_da_pagina = "Stream Interativa - Streamer - Painel";

//INICIA SESSÕES NO SISTEMA
session_start();

//VERIFICA SE O USUÁRIO JÁ ESTÁ AUTENTICADO
if (!isset($_SESSION["id_usuario"]))
{
	//RETORNA PARA A TELA DE ERRO COM O CÓDIGO DO ERRO
	header('Location: home_entrar.php?erro=autenticacao');
}
//SE O USUÁRIO JÁ ESTÁ AUTENTICADO
else
{
	//INCLUI AS VARIÁVEIS DE ACESSO AO BANCO DE DADOS
	include ('include/acesso_bd.php');
	
	//INCLUI AS FUNÇÕES GLOBAIS DO SISTEMA
	include ('include/funcoes.php');

	//CAPTURA O ID DO USUÁRIO NAS SESSÕES DO SISTEMA
	$id_usuario = $_SESSION["id_usuario"];

	//QUERY PARA CONSULTAR OS DADOS CADASTRADOS DO STREAMER
	$query_select = "
	SELECT 
		email, nm_jogo, nm_usuario_jogo
	FROM 
		stin_usuarios U
		LEFT JOIN stin_jogos J ON (U.id_usuario = J.id_usuario) 
	WHERE 
		U.id_usuario = '".$id_usuario."'";

	//EXECUTA A CONSULTA NO BANCO DE DADOS
	$select = mysql_query($query_select,$connect);

	//ARMAZENA OS DADOS DA CONSULTA EM UM ARRAY
	$array = mysql_fetch_array($select);

	//CAPTURA EM VARIÁVEIS OS DADOS DO BANCO DE DADOS
	$email 				= $array['email'];
	$nm_jogo 			= $array['nm_jogo'];
	$nm_usuario_jogo 	= $array['nm_usuario_jogo'];
	
	//###################################################
	//QUERY PARA CONSULTAR TODOS OS STREAMERS CADASTRADOS
	$query_select = "
	SELECT 
		U.id_usuario, J.nm_usuario_jogo
	FROM 
		stin_usuarios U
		LEFT JOIN stin_jogos J ON (U.id_usuario = J.id_usuario) 
		LEFT JOIN stin_canais C ON (U.id_usuario = C.id_usuario)
		LEFT JOIN stin_tr_usuarios_tipos_usuarios TR_UTU ON (U.id_usuario = TR_UTU.id_usuario)
	WHERE 
		TR_UTU.id_tipo_usuario = 1";

	//EXECUTA A CONSULTA NO BANCO DE DADOS
	$select_streamers = mysql_query($query_select, $connect);
	
	//###################################################
	//CAPTURA O ID DO STREAMER SE FOR REPASSADO PELA URL
	if (isset($_GET['id_streamer']) && $_GET['id_streamer'] != '')
	{
		//ARMAZENA O IDENTIFICADOR DO STREAMER
		$id_streamer = $_GET['id_streamer'];
				
		//VERIFICA SE O USUÁRIO SUGERIU A LANE
		if (isset($_GET["acao"]) && $_GET["acao"] == 'sugerir_lane')
		{
			//CAPTURA OS DADOS EVIADOS PELO FORMULÁRIO
			$lane_principal = (isset($_POST["lane_principal"]) && $_POST["lane_principal"] != '') ? addslashes(trim($_POST["lane_principal"])) : $lane_principal = "";
			$lane_secundaria = (isset($_POST["lane_secundaria"]) && $_POST["lane_secundaria"] != '') ? addslashes(trim($_POST["lane_secundaria"])) : $lane_secundaria = "";		
		}
		
		//QUERY PARA VERIFICAR SE O STREAMER POSSUI ALGUMA PARTIDA ATIVA
		$query_select = "SELECT id_partida FROM stin_partidas WHERE id_streamer = ".$id_streamer." AND st_partida = 1";

		//EXECUTA A CONSULTA NO BANCO DE DADOS
		$select = mysql_query($query_select,$connect);
		
		//VERIFICA SE HÁ PARTIDA ATIVA
		if (mysql_num_rows($select))
		{
			//REGISTRA EM VARIÁVEL QUE HÁ PARTIDA ATIVA
			$partida_status = "ONLINE";
			
			//ARMAZENA OS DADOS DA CONSULTA EM UM ARRAY
			$array = mysql_fetch_array($select);

			//CAPTURA EM VARIÁVEIS OS DADOS DO BANCO DE DADOS
			$id_partida = $array['id_partida'];

			//CAPTURA A AÇÃO PARA SUGESTÃO DAS LANES
			if (isset($_GET['acao']) && $_GET['acao'] != '')
			{
				//CAPTURA AS LANES SUGERIDAS PELO VIEWER
				$id_partida = $_GET['id_partida'];
				$id_lane_principal = $_POST['id_lane_principal'];
				$id_lane_secundaria = $_POST['id_lane_secundaria'];
				
				//CRIA A QUERY PARA REGISTRAR NO BANCO A SUGESTÃO DO VIEWER
				$query_insert = "
				INSERT INTO stin_sugestoes_lanes 
				(
					id_partida,
					id_viewer,
					id_lane_principal,
					id_lane_secundaria
				)
				VALUES
				(
					".$id_partida.",
					".$id_usuario.",
					".$id_lane_principal.",
					".$id_lane_secundaria."
				)";
				
				//EXECUTA A INCLUSÃO NO BANCO DE DADOS
				mysql_query($query_insert, $connect);
			}
			//SE O VIEWER AINDA NÃO SELECIONOU A AÇÃO
			else
			{
				//CRIA COMO NULAS AS LANES SUGERIDAS PELO VIEWER
				$id_lane_principal = NULL;
				$id_lane_secundaria = NULL;
			}
			
			//FUNÇÃO PARA CONSULTAR TODAS AS SUGESTÕES DE LANE
			$select_sugestoes_lanes = F_ConsultarSugestaoDeLanes ($id_partida, $connect);
		}
		//QUANDO NÃO HÁ PARTIDA ATIVA
		else
		{
			//REGISTRA EM VARIÁVEL QUE NÃO HÁ PARTIDA ATIVA
			$partida_status = "OFFLINE";		
		}
	}
	//SE O ID DO STREAMER NÃO FOR REPASSADO PELA URL
	else 
	{
		//ARMAZENA O IDENTIFICADOR DO STREAMER
		$id_streamer = "";
	}
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include ('layout_head.php'); ?>
</head>
<body id="page-top">
	<?php include ('layout_menu.php'); ?>
    <section class="bg-primary">
        <div class="container">
			<div class="row">
				<div class="col-md-12">
					<h1 class="section-heading text-center">PAINEL DO VIEWER</h1>
					<p class="text-center">
						Por meio desse Painel será possível utilizar as funcionalidades do sistemas de interação.<br/>	
						Ecolha um Streamer para iniciar o processo de interação por meio da plataforma.
					</p>
					<hr class="light">
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">								
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="index.php">Home</a></li>
						<li class="breadcrumb-item"><a href="viewer_inicio.php">Viewer</a></li>
						<li class="breadcrumb-item active">Painel</li>
					</ol>					
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title"><strong>Selecione um Streamer</strong></h3>
						</div>
						<div class="panel-body">						
							<select class="form-control" name="streamer" onchange="CarregarStreamer(this.value)">
								<option value="">( Selecione )</option>
								<?php
								//PRENCHE O COMBO
								while ($linha = mysql_fetch_assoc($select_streamers))
								{
									//VERIFICA SE O ID DO STREAMER É O MESMO PASSADO VIA URL
									if ($linha['id_usuario'] == $id_streamer)
									{
										//APRESENTA O STREAMER SELECIONADO
										echo '<option value="'.$linha['id_usuario'].'" selected>'.$linha['nm_usuario_jogo'].'</option>';
									}
									else
									{
										//APRESENTA O STREAMER PARA SELEÇÃO
										echo '<option value="'.$linha['id_usuario'].'">'.$linha['nm_usuario_jogo'].'</option>';
									}
								}
								?>
							</select>
						</div>
					</div>
				</div>				
			</div>
			
			<?php
			//VERIFICAR SE FOI REPASSADO O ID DO STREAMER
			if ($id_streamer != "")
			{
				//VERIFICA SE O STREAMER POSSUI ALGUMA PARTIDA ATIVA
				if($partida_status == "ONLINE")
				{				
				?>			
					<div class="row">
						<div class="col-md-3">
							<div class="panel panel-default">
								<div class="panel-heading text-center">							
									<h3 class="panel-title"><strong>Sugestão de Lane</strong></h3>
								</div>
								<div class="panel-body">
									<ul class="list-unstyled text-primary">							
										<?php								
											//APRESENTA AS SUGESTÕES DE LANE
											while ($linha_lanes = mysql_fetch_assoc($select_sugestoes_lanes))
											{
												//VERIFICA SE O VIEWER SELECIONOU ESTA LANE COMO PRINCIPAL
												if ($linha_lanes['id_lane_principal'] == $id_lane_principal)
												{
													//APRESENTA A LANE MARCADA COMO PRINCIPAL										
													echo "<li><strong>".$linha_lanes['porcentagem_principal']." ".$linha_lanes['nm_lane']."</strong> <i class='fa fa-hand-o-left text-primary'></i></li>";
												}
												//VERIFICA SE O VIEWER SELECIONOU ESTA LANE COMO SECUNDÁRIA
												else if ($linha_lanes['id_lane_principal'] == $id_lane_secundaria)
												{
													//APRESENTA A LANE MARCADA COMO SECUNDÁRIA										
													echo "<li><strong>".$linha_lanes['porcentagem_principal']." ".$linha_lanes['nm_lane']."</strong> <i class='fa fa-hand-o-left text-primary'></i></li>";
												}
												//SE O VIEWER NÃO SELECIONOU ESTA LANE
												else
												{
													//APRESENTA A LANE
													echo "<li><strong>".$linha_lanes['porcentagem_principal']." ".$linha_lanes['nm_lane']."</strong></li>";
												}
											}
										?>
									</ul>							
									<button type="button" class="btn btn-primary" onclick="window.location.href='viewer_sugerir_lane.php?id_streamer=<?=$id_streamer?>'">
										<span class="fa fa-circle" aria-hidden="true"></span> Sugerir
									</button>							
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h3 class="panel-title text-center"><strong>Sugestão de Champion</strong></h3>
								</div>
								<div class="panel-body">							
									<ul class="list-unstyled text-primary">
										<li><strong>60%: Garen</strong></li>
										<li><strong>15%: Master Yii</strong></li>
										<li><strong>10%: Leona</strong></li>
										<li><strong>5%: Lux</strong></li>
										<li><strong>5%: Jax</strong></li>
										<li><strong>1%: Zed</strong></li>
									</ul>
									<button type="button" class="btn btn-primary" onclick="window.location.href='index.php'">
										<span class="fa fa-check-circle" aria-hidden="true"></span> Sugerido
									</button>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h3 class="panel-title text-center"><strong>Sugestão de Build</strong></h3>
								</div>
								<div class="panel-body">
									<ul class="list-unstyled text-primary">
										<li><strong>Anjo Guardião</strong></li>
										<li><strong>Glória Íntegra</strong></li>
										<li><strong>Lâmina de Youmuu</strong></li>
										<li><strong>O Cutelo Negro</strong></li>
										<li><strong>Couraça do Defunto</strong></li>
										<li><strong>Passos de Mercúrio</strong></li>
									</ul>
									<button type="button" class="btn btn-primary">
										<span class="fa fa-handshake-o" aria-hidden="true"></span> Sugerir
									</button>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h3 class="panel-title text-center"><strong>Sugestão de Target</strong></h3>
								</div>
								<div class="panel-body">
									<ul class="list-unstyled text-primary">
										<li><strong>Multi Kill: Quadra</strong></li>
										<li><strong>First Blood: Sim</strong></li>
										<li><strong>Dano a campeões >= 10k</strong></li>
										<li><strong>Dano causado >= 500K</strong></li>
										<li><strong>Maior crítico >= 1K</strong></li>
										<li><strong>Sentinelas destruídas >= 5</strong></li>
									</ul>							
									<button type="button" class="btn btn-primary">
										<span class="fa fa-handshake-o" aria-hidden="true"></span> Sugerir
									</button>
								</div>
							</div>
						</div>
					</div>	
				<?php
				}
				//SE O STREAMER NÃO POSSUI NENHUM PARTIDA ATIVA
				else if($partida_status == "OFFLINE")
				{
					?>
					<div class="row">
						<div class="col-md-12 text-center">
							<a href="streamer_painel.php?acao=criar_partida" class="page-scroll btn btn-default btn-xl sr-button">CRIAR PARTIDA</a>
						</div>
					</div>				
					<?php
				}
			}
			?>			
        </div>
    </section>
	<?php include ('layout_footer.php'); ?>
	<?php include ('layout_scripts.php'); ?>

	<!-- Funções JavaScript -->
	<script  type="text/javascript">
	
		//Função para Carregar o Streamer
		function CarregarStreamer(id_streamer)
		{
			//Verifica se o Identificador do Streamer foi repassado pelo Seletor
			if (id_streamer != '')
			{
				//Carrega a página passando o parâmetro do Streamer
				window.location.assign("viewer_painel.php?id_streamer=" + id_streamer)
			}
			//Se o Identificador do Streamer não foi repassado pelo Seletor
			else
			{
				//Carrega a página passando o parâmetro do Streamer
				window.location.assign("viewer_painel.php")
			}
		}
	
	</script>
	
</body>
</html>



	