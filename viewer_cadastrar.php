<?php
//DEFINIÇÃO DO TÍTULO DA PÁGINA
$titulo_da_pagina = "Stream Interativa - Viewer - Cadastrar";

//INICIA SESSÕES NO SISTEMA
session_start();

//CAPTURA ERRO SE EXISTIR
$erro = (isset($_GET["erro"]) && $_GET["erro"] != '') ? addslashes(trim($_GET["erro"])) : $erro = "";

//VERIFICA SE FOI ENVIADA A AÇÃO DE ADICIONAR O REGISTRO
if (isset($_GET["acao"]) && $_GET["acao"] == 'adicionar')
{
	//INCLUI AS VARIÁVEIS DE ACESSO AO BANCO DE DADOS
	include ('include/acesso_bd.php');

	//CAPTURA OS DADOS EVIADOS PELO FORMULÁRIO
	$email = (isset($_POST["email"]) && $_POST["email"] != '') ? addslashes(trim($_POST["email"])) : $email = "";
	$senha = (isset($_POST["senha"]) && $_POST["senha"] != '') ? addslashes(trim($_POST["senha"])) : $senha = "";
	$repetir_senha = (isset($_POST["repetir_senha"]) && $_POST["repetir_senha"] != '') ? addslashes(trim($_POST["repetir_senha"])) : $repetir_senha = "";
	$nm_jogo = (isset($_POST["nm_jogo"]) && $_POST["nm_jogo"] != '') ? addslashes(trim($_POST["nm_jogo"])) : $nm_jogo = "";
	$nm_usuario_jogo = (isset($_POST["nm_usuario_jogo"]) && $_POST["nm_usuario_jogo"] != '') ? addslashes(trim($_POST["nm_usuario_jogo"])) : $nm_usuario_jogo = "";

	//VERIFICA SE OS DADOS OBRIGATÓRIOS NÃO FORAM INFORMADOS
	if ($email == "" || $senha == "")
	{	
		//RETORNA PARA A TELA DE ERRO COM O CÓDIGO RELATIVO
		$erro = "dados_vazios";
	}
	//SE OS DADOS OBRIGATÓRIOS FORAM INFORMADOS
	else
	{
		//QUERY PARA VERIFICAR SE O EMAIL INFORMADO JÁ EXISTE NO BANCO DE DADOS
		$query_select = "SELECT email FROM stin_usuarios WHERE email = '".$email."'";

		//EXECUTA A QUERY NO BANCO DE DADOS
		$select = mysql_query($query_select, $connect);

		//VERIFICA SE O EMAIL INFORMADO JÁ EXISTE NO BANCO DE DADOS
		if (mysql_num_rows($select))
		{
			//RETORNA PARA A TELA DE ERRO COM O CÓDIGO RESPECTIVO
			$erro = "dados_existentes";
		}
		//O EMAIL INFORMADO NÃO EXISTE NO BANCO DE DADOS
		else
		{
			//CRIPTOGRAFA A SENHA PARA UTILIZAÇÃO ADEQUADA
			$senha = MD5($senha);
			
			//INICIA A TRANSAÇÃO NO BANCO DE DADOS
			mysql_query("BEGIN", $connect);

			//CRIA A QUERY PARA REALIZAR O CADASTRO DO USUÁRIO NO SISTEMA
			$query_insert = "INSERT INTO stin_usuarios (email,senha) VALUES ('".$email."','".$senha."')";
			
			//EXECUTA A QUERY NO BANCO DE DADOS
			$insert = mysql_query($query_insert, $connect);
			
			//VERIFICA SE A INCLUSÃO NÃO FOI REALIZADA COM SUCESSO
			if(!$insert)
			{
				//DESFAZ A TRANSAÇÃO NO BANCO DE DADOS
				mysql_query("ROLLBACK", $connect);

				//RETORNA PARA A TELA DE ERRO COM O CÓDIGO RESPECTIVO
				$erro = "incluir_usuario";
			}
			//SE A INCLUSÃO FOI REALIZADA COM SUCESSO
			else
			{
				//CRIA QUERY PARA CAPTURAR O ID DO USUÁRIO ADICIONADO NO BANCO DE DADOS
				$query_select = "SELECT LAST_INSERT_ID() AS id_usuario";
				
				//EXECUTA A QUERY NO BANCO DE DADOS
				$select = mysql_query($query_select, $connect);
				
				//ARMAZENA OS DADOS DA CONSULTA EM UM ARRAY
				$array = mysql_fetch_assoc($select);
				
				//CAPTURA O ID DO USUÁRIO ADICIONADO NO BANCO DE DADOS
				$id_usuario = $array['id_usuario'];
				
				//DEFINE O TIPO DE USUÁRIO A SER CADASTRADO NO BANCO DE DADOS (2 = VIEWER)
				$id_tipo_usuario = 2;
				
				//CRIA QUERY PARA ARMAZENAR O TIPO DE USUÁRIO
				$query_insert = "INSERT INTO stin_tr_usuarios_tipos_usuarios (id_usuario, id_tipo_usuario) VALUES (".$id_usuario.", ".$id_tipo_usuario.")";
				
				//EXECUTA A QUERY NO BANCO DE DADOS
				$insert = mysql_query($query_insert, $connect);
				
				//VERIFICA SE A INCLUSÃO NÃO FOI REALIZADA COM SUCESSO
				if(!$insert)
				{
					//DESFAZ A TRANSAÇÃO NO BANCO DE DADOS
					mysql_query("ROLLBACK", $connect);

					//RETORNA PARA A TELA DE ERRO COM O CÓDIGO RESPECTIVO
					$erro = "incluir_tipo_usuario";
				}
				//SE A INCLUSÃO FOI REALIZADA COM SUCESSO
				else
				{
					//VERIFICA SE OS DADOS DO JOGO FORAM INFORMADOS
					if ($nm_jogo != "" && $nm_usuario_jogo != "")
					{
						//QUERY PARA CADASTRAR OS DADOS DE JOGO NO BANCO DE DADOS
						$query_insert = "
						INSERT INTO stin_jogos (id_usuario, nm_jogo, nm_usuario_jogo) 
						VALUES (".$id_usuario.", '".$nm_jogo."', '".$nm_usuario_jogo."')";
						
						//EXECUTA A QUERY NO BANCO DE DADOS
						$insert = mysql_query($query_insert, $connect);
						
						//VERIFICA SE A INCLUSÃO NÃO FOI REALIZADA COM SUCESSO
						if(!$insert)
						{	
							//DESFAZ A TRANSAÇÃO NO BANCO DE DADOS
							mysql_query("ROLLBACK", $connect);

							//RETORNA PARA A TELA DE ERRO COM O CÓDIGO RESPECTIVO
							$erro = "incluir_dados_jogo";
						}
					}
					
					//FINALIZA A TRANSAÇÃO NO BANCO DE DADOS
					mysql_query("COMMIT", $connect);
										
					//CRIA SESSÕES DE AUTENTICAÇÃO DO USUÁRIO
					$_SESSION["id_usuario"]		= $id_usuario; 
					$_SESSION["email"] 			= $email; 
					
					//QUERY DE CAPTURA DOS TIPOS DO USUÁRIO
					$query_select = "SELECT id_tipo_usuario FROM stin_tr_usuarios_tipos_usuarios WHERE id_usuario = ".$id_usuario."";		
					
					//EXECUTA A CONSULTA NO BANCO DE DADOS
					$select_tr_usuarios_tipos_usuarios = mysql_query($query_select, $connect);
					
					//ARMAZENA OS TIPOS DO USUÁRIO EM SESSÃO
					while ($stin_tr_usuarios_tipos_usuarios = mysql_fetch_assoc($select_tr_usuarios_tipos_usuarios))
					{
						//ARMAZENA O TIPO DE USUÁRIO EM FORMA DE VETOR
						$_SESSION["id_tipo_usuario"][] = $stin_tr_usuarios_tipos_usuarios["id_tipo_usuario"]; 
					}
					
					//ENVIA PARA A TELA DE INICIO DE UTILIZACAO
					header('Location: viewer_inicio.php');
				}
			}
		}
	}
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include ('layout_head.php'); ?>
</head>
<body id="page-top">
	<?php include ('layout_menu.php'); ?>
	<section class="bg-primary">
        <div class="container">
			<div class="row">
				<div class="col-md-12">
					<h1 class="section-heading text-center">INFORME OS SEUS DADOS PARA CADASTRAR-SE</h1>
					<p class="text-center">Faça o cadastro informando o maior número de informações possíveis.<br/>Dessa forma poderemos organizar a plataforma mais adequadadamente para te auxiliar.</p>
					<hr class="light">
					
					<?php
					//VERIFICA SE HOUVE ERRO: FALTOU INFORMAR EMAIL OU SENHA
					if ($erro == "dados_vazios") 
					{
					?>
						<div class="alert alert-danger">
							<strong>Atenção!</strong> Informe o Email e a Senha para cadastrar o Usuário.
						</div>	
					<?php
					}
					//VERIFICA SE HOUVE ERRO: DADOS JÁ EXISTEM NO BANCO DE DADOS
					else if ($erro == "dados_existentes") 
					{
					?>
						<div class="alert alert-danger">
							<strong>Atenção!</strong> O Email informado já existe no Banco de Dados.
						</div>	
					<?php
					}
					//VERIFICA SE HOUVE ERRO: ERRO AO CADASTRAR O USUÁRIO NO BANCO DE DADOS
					else if ($erro == "incluir_usuario") 
					{
					?>
						<div class="alert alert-danger">
							<strong>Atenção!</strong> Erro ao cadastrar o Usuário no Banco de Dados.
						</div>	
					<?php
					}
					//VERIFICA SE HOUVE ERRO: ERRO AO CADASTRAR O TIPO DE USUÁRIO NO BANCO DE DADOS
					else if ($erro == "incluir_tipo_usuario") 
					{
					?>
						<div class="alert alert-danger">
							<strong>Atenção!</strong> Erro ao cadastrar o Tipo do Usuário no Banco de Dados.
						</div>	
					<?php
					}
					//VERIFICA SE HOUVE ERRO: ERRO AO CADASTRAR OS DADOS DO JOGO NO BANCO DE DADOS
					else if ($erro == "incluir_dados_jogo") 
					{
					?>
						<div class="alert alert-danger">
							<strong>Atenção!</strong> Erro ao cadastrar os Dados do Jogo no Banco de Dados.
						</div>	
					<?php
					}
					?>
				</div>
			</div>			
			<form action="viewer_cadastrar.php?acao=adicionar" method="post">
				<div class="row">
					<div class="col-md-12">					
						<div class="form-group">
							<label for="email">Email *</label>
							<input type="email" class="form-control text-lowercase" id="email" name="email" placeholder="Informe um Email válido" required>
						</div>
					</div>	
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label for="senha">Senha *</label>
							<input type="password" class="form-control" id="senha" name="senha" placeholder="Informe uma Senha para acesso" required>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label for="senha">Repetir Senha *</label>
							<input type="password" class="form-control" id="repetir_senha" name="repetir_senha" placeholder="Repita a Senha informada" required>
						</div>
					</div>
				</div>				
				<div class="row">
					<div class="col-md-6">							
						<div class="form-group">
							<label for="email">Jogo</label>
							<select class="form-control" name="nm_jogo">
								<option value="">( Selecione )</option>
								<option value="League of Legends">League of Legends</option>
							</select>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label for="nm_usuario_jogo">Usuário do Jogo</label>
							<input type="text" class="form-control" id="nm_usuario_jogo" name="nm_usuario_jogo" placeholder="Informe o seu Usuário do Jogo">
						</div>
					</div>
				</div>					
				<div class="row">
					<div class="col-md-12">
						<button type="submit" class="btn btn-default">Enviar</button>
					</div>
				</div>
			</form>
        </div>
    </section>
    <?php include ('layout_footer.php'); ?>
	<?php include ('layout_scripts.php'); ?>	
</body>
</html>