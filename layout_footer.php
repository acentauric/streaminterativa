<section id="footer">
	<div class="container">
		<div class="row">
			<div class="col-lg-8 col-lg-offset-2 text-center">
				<h2 class="section-heading">Dúvidas, Sugestões ou Reclamações?</h2>
				<hr class="primary">
				<p>
					Nós valorizamos muito a sua opnião!<br/>
					Será por meio dela que construiremos uma plataforma cada vez melhor!
				</p>
			</div>
			<div class="col-lg-4 col-lg-offset-2 text-center">
				<i class="fa fa-whatsapp fa-3x sr-contact"></i>
				<p>(71) 99369-9538</p>
			</div>
			<div class="col-lg-4 text-center">
				<i class="fa fa-envelope-o fa-3x sr-contact"></i>
				<p><a href="mailto:contato@alphacentauri.com.br">contato@alphacentauri.com.br</a></p>
			</div>
		</div>
	</div>
</section>