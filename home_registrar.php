<?php
//DEFINIÇÃO DO TÍTULO DA PÁGINA
$titulo_da_pagina = "Stream Interativa - Home - Registrar";
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include ('layout_head.php'); ?>
</head>
<body id="page-top">
    <?php include ('layout_menu.php'); ?>
    <section class="bg-primary">
        <div class="container">
			<div class="row">
				<div class="col-md-12">
					<h1 class="section-heading text-center">DEFINA O SEU PERFIL</h1>
					<p class="text-center">
						Precisamos te conhecer para oferecer a melhor experiência de acordo com o seu tipo de utilização da plataforma.<br/>
						Não se preocupe! Você poderá alterar o seu perfil e os seus dados posteriormente.
					</p>
					<hr class="light">
				</div>
			</div>
			<div class="row">
				<div class="col-md-6 text-center">					
					<i class="fa fa-4x fa-microphone"></i>
					<h3>Streamer</h3>
					<p class="text-faded">Este perfil permitirá que você intereja com os Viewers. Você receberá sugestões deles enquanto estiver stremando.</p>
					<a href="streamer_cadastrar.php" class="page-scroll btn btn-default btn-xl sr-button">Cadastrar-se</a>
				</div>
				<div class="col-md-6 text-center">					
					<i class="fa fa-4x fa-comments"></i>
					<h3>Viewer</h3>
					<p class="text-faded">Este perfil permitirá que você intereja com os Streamers. Você enviará sugestões para eles enquanto estiverem stremando.</p>
					<a href="viewer_cadastrar.php" class="page-scroll btn btn-default btn-xl sr-button">Cadastrar-se</a>
				</div>				
			</div>				
        </div>
    </section>
    <?php include ('layout_footer.php'); ?>
	<?php include ('layout_scripts.php'); ?>	
</body>
</html>