<?php
//DEFINIÇÃO DO TÍTULO DA PÁGINA
$titulo_da_pagina = "Stream Interativa - Streamer - Painel";

//INICIA SESSÕES NO SISTEMA
session_start();

//VERIFICA SE O USUÁRIO JÁ ESTÁ AUTENTICADO
if (!isset($_SESSION["id_usuario"]))
{
	//RETORNA PARA A TELA DE ERRO COM O CÓDIGO DO ERRO
	header('Location: home_entrar.php?erro=autenticacao');
}
//SE O USUÁRIO JÁ ESTÁ AUTENTICADO
else
{	
	//INCLUI AS VARIÁVEIS DE ACESSO AO BANCO DE DADOS
	include ('include/acesso_bd.php');

	//CAPTURA O ID DO STREAMER NAS SESSÕES DO SISTEMA
	$id_usuario = $_SESSION["id_usuario"];

	//QUERY PARA CONSULTAR OS DADOS CADASTRADOS DO STREAMER
	$query_select = "
	SELECT 
		email, nm_jogo, nm_usuario_jogo
	FROM 
		stin_usuarios U
		LEFT JOIN stin_jogos J ON (U.id_usuario = J.id_usuario) 
	WHERE 
		U.id_usuario = '".$id_usuario."'";

	//EXECUTA A CONSULTA NO BANCO DE DADOS
	$select = mysql_query($query_select,$connect);

	//ARMAZENA OS DADOS DA CONSULTA EM UM ARRAY
	$array = mysql_fetch_array($select);

	//CAPTURA EM VARIÁVEIS OS DADOS DO BANCO DE DADOS
	$email 				= $array['email'];
	$nm_jogo 			= $array['nm_jogo'];
	$nm_usuario_jogo 	= $array['nm_usuario_jogo'];
	
	//###################################################
	//CAPTURA O ID DO STREAMER SE FOR REPASSADO PELA URL
	if (isset($_GET['id_streamer']) && $_GET['id_streamer'] != ''){$id_streamer = $_GET['id_streamer'];}else {$id_streamer = "";}
	
	//CONSULTA OS DADOS DO STREAMER PARA APRESENTAR NA TELA
	$query_select = "
	SELECT 
		J.nm_jogo, J.nm_usuario_jogo, C.nm_canal, C.nm_link_canal 
	FROM 
		stin_usuarios U
		LEFT JOIN stin_jogos J ON (U.id_usuario = J.id_usuario) 
		LEFT JOIN stin_canais C ON (U.id_usuario = C.id_usuario)		
	WHERE 
		U.id_usuario = ".$id_streamer."";

	//EXECUTA A CONSULTA NO BANCO DE DADOS
	$select_streamer = mysql_query($query_select, $connect);
	
	//ARMAZENA OS DADOS DA CONSULTA EM UM ARRAY
	$streamer = mysql_fetch_array($select_streamer);

	//CAPTURA EM VARIÁVEIS OS DADOS DO BANCO DE DADOS	
	$streamer_nm_jogo 			= $streamer['nm_jogo'];
	$streamer_nm_usuario_jogo 	= $streamer['nm_usuario_jogo'];
	$streamer_nm_canal 			= $streamer['nm_canal'];
	$streamer_nm_link_canal 	= $streamer['nm_link_canal'];
	
	//###################################################
	//QUERY PARA CONSULTAR TODAS AS LANES DISPONÍVEIS
	$query_select_lanes = "SELECT  id_lane, nm_lane FROM stin_lanes";
	
	//###################################################
	//QUERY PARA VERIFICAR SE O STREAMER POSSUI ALGUMA PARTIDA ATIVA
	$query_select_partida = "SELECT id_partida FROM stin_partidas WHERE id_streamer = ".$id_streamer." AND st_partida = 1";

	//EXECUTA A CONSULTA NO BANCO DE DADOS
	$select = mysql_query($query_select_partida,$connect);
	
	//VERIFICA SE HÁ PARTIDA ATIVA
	if (mysql_num_rows($select))
	{
		//REGISTRA EM VARIÁVEL QUE HÁ PARTIDA ATIVA
		$partida_status = "ONLINE";
		
		//ARMAZENA OS DADOS DA CONSULTA EM UM ARRAY
		$array = mysql_fetch_array($select);

		//CAPTURA EM VARIÁVEIS OS DADOS DO BANCO DE DADOS
		$id_partida = $array['id_partida'];
	}
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include ('layout_head.php'); ?>
</head>
<body id="page-top">
	<?php include ('layout_menu.php'); ?>
    <section class="bg-primary">
        <div class="container">
			<div class="row">
				<div class="col-md-12">
					<h1 class="section-heading text-center">PAINEL DO VIEWER</h1>
					<p class="text-center">
						Por meio desse Painel será possível utilizar as funcionalidades do sistemas de interação.<br/>	
						Ecolha um Streamer para iniciar o processo de interação por meio da plataforma.
					</p>
					<hr class="light">
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">								
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="index.php">Home</a></li>
						<li class="breadcrumb-item"><a href="viewer_inicio.php">Viewer</a></li>
						<li class="breadcrumb-item"><a href="viewer_painel.php">Painel</a></li>
						<li class="breadcrumb-item active">Sugerir Lane</li>
					</ol>					
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title"><strong>Dados do Streamer</strong></h3>
						</div>
						<div class="panel-body">																		
							<ul class="list-unstyled text-primary">
								<li><strong>Jogo:</strong> <?=$streamer_nm_jogo?></li>
								<li><strong>Usuário do Jogo:</strong> <?=$streamer_nm_usuario_jogo?></li>                        							
								<li><strong>Canal:</strong> <?=$streamer_nm_canal?></li>
								<li><strong>Link do Canal:</strong> <a href="<?=$nm_link_canal?>" target="_blank"><?=$streamer_nm_link_canal?></a></li>
								<li><strong>Partida:</strong> <?=$id_partida?></li>
							</ul>							
						</div>
					</div>
				</div>				
			</div>	
			<form id="form" action="viewer_painel.php?id_streamer=<?=$id_streamer?>&id_partida=<?=$id_partida?>&acao=sugerir_lanes" method="post">
				<div class="row">
					<div class="col-md-6">
						<div class="panel panel-default">
							<div class="panel-heading">							
								<h3 class="panel-title"><strong>Selecione a Lane Principal</strong></h3>
							</div>
							<div class="panel-body">							
								<select class="form-control" name="id_lane_principal">
									<option value="">( Selecione )</option>
									<?php
									//EXECUTA A CONSULTA NO BANCO DE DADOS
									$select_lanes = mysql_query($query_select_lanes, $connect);
									
									//PRENCHE O COMBO
									while ($linha = mysql_fetch_assoc($select_lanes))
									{
										//APRESENTA O STREAMER SELECIONADO
										echo '<option value="'.$linha['id_lane'].'">'.$linha['nm_lane'].'</option>';
									}
									?>
								</select>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="panel panel-default">
							<div class="panel-heading">							
								<h3 class="panel-title"><strong>Selecione a Lane Secundária</strong></h3>
							</div>
							<div class="panel-body">							
								<select class="form-control" name="id_lane_secundaria">
									<option value="">( Selecione )</option>
									<?php
									//EXECUTA A CONSULTA NO BANCO DE DADOS
									$select_lanes = mysql_query($query_select_lanes, $connect);
									
									//PRENCHE O COMBO
									while ($linha = mysql_fetch_assoc($select_lanes))
									{
										//APRESENTA O STREAMER SELECIONADO
										echo '<option value="'.$linha['id_lane'].'">'.$linha['nm_lane'].'</option>';
									}
									?>
								</select>							
							</div>
						</div>
					</div>					
				</div>	
				<div class="row">
					<div class="col-md-12">
						<button type="submit" class="btn btn-default">Enviar</button>
					</div>
				</div>
			</form>				
        </div>
    </section>
	<?php include ('layout_footer.php'); ?>
	<?php include ('layout_scripts.php'); ?>

	<!-- Funções JavaScript -->
	<script  type="text/javascript">
	
		//Função para Carregar o Streamer
		function CarregarStreamer(id_streamer)
		{
			//Verifica se o Identificador do Streamer foi repassado pelo Seletor
			if (id_streamer != '')
			{
				//Carrega a página passando o parâmetro do Streamer
				window.location.assign("viewer_painel.php?id_streamer=" + id_streamer)
			}
			//Se o Identificador do Streamer não foi repassado pelo Seletor
			else
			{
				//Carrega a página passando o parâmetro do Streamer
				window.location.assign("viewer_painel.php")
			}
		}
	
	</script>
	
</body>
</html>