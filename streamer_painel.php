<?php
//DEFINIÇÃO DO TÍTULO DA PÁGINA
$titulo_da_pagina = "Stream Interativa - Streamer - Painel";

//INICIA SESSÕES NO SISTEMA
session_start();

//VERIFICA SE O USUÁRIO JÁ ESTÁ AUTENTICADO
if (!isset($_SESSION["id_usuario"]))
{
	//RETORNA PARA A TELA DE ERRO COM O CÓDIGO DO ERRO
	header('Location: home_entrar.php?erro=autenticacao');
}
//SE O USUÁRIO JÁ ESTÁ AUTENTICADO
else
{
	//INCLUI AS VARIÁVEIS DE ACESSO AO BANCO DE DADOS
	include ('include/acesso_bd.php');
	
	//INCLUI AS FUNÇÕES GLOBAIS DO SISTEMA
	include ('include/funcoes.php');

	//CAPTURA O ID DO STREAMER NAS SESSÕES DO SISTEMA
	$id_usuario = $_SESSION["id_usuario"];

	//QUERY PARA CONSULTAR OS DADOS CADASTRADOS DO STREAMER
	$query_select = "
	SELECT 
		email, nm_jogo, nm_usuario_jogo, nm_canal, nm_link_canal 
	FROM 
		stin_usuarios U
		LEFT JOIN stin_jogos J ON (U.id_usuario = J.id_usuario) 
		LEFT JOIN stin_canais C ON (U.id_usuario = C.id_usuario) 
	WHERE 
		U.id_usuario = '".$id_usuario."'";

	//EXECUTA A CONSULTA NO BANCO DE DADOS
	$select = mysql_query($query_select,$connect);

	//ARMAZENA OS DADOS DA CONSULTA EM UM ARRAY
	$array = mysql_fetch_array($select);

	//CAPTURA EM VARIÁVEIS OS DADOS DO BANCO DE DADOS
	$email 				= $array['email'];
	$nm_jogo 			= $array['nm_jogo'];
	$nm_usuario_jogo 	= $array['nm_usuario_jogo'];
	$nm_canal 			= $array['nm_canal'];
	$nm_link_canal 		= $array['nm_link_canal'];

	//CRIA UMA VARIÁVEL PARA ARMAZENAR O STATUS DA TRANSMISSÃO
	$ds_status_da_transmissao = "";

	//###############################################################################
	//CRAWLER PARA SABER SE O CANAL ESTÁ AO VIVO
	
	//Define a URL do Site que será acessando para realizar o Crawler
	$file = $nm_link_canal."/live";

	//Instancia um novo objeto do tipo DOMDocument para o devido tratamento dos caracteres
	$doc = new \DOMDocument('1.0', 'UTF-8');

	//Set error level
	$internalErrors = libxml_use_internal_errors(true);

	//Se for possível carregar o arquivo HTML utilizando o método do DOMDocument
	if(@$doc->loadHTMLFile($file))
	{
		//Restore error level
		libxml_use_internal_errors($internalErrors);

		//Instancia o objeto xPath a partir do arquivo HTML carregado
		$xpath = new \DOMXpath($doc);

		//Define qual informação será coletada pelo xPath
		$xPathQuery = '//*[@id="watch-uploader-info"]/strong';

		//Executa a query xPath e armazena o resultado em um Array
		$elements = $xpath->query($xPathQuery);

		//Verifica se a query utilizada retornou elementos
		if (!is_null($elements))
		{
			//Percorre todos os elementos retornados
			foreach ($elements as $element)
			{
				//Captura todos os childNodes do Elemento
				$nodes = $element->childNodes;
				
				//Percorre todos os childNodes capturados
				foreach ($nodes as $node)
				{
					//Armana o valor do node em uma variável (remove espaços em branco)
					$nodeValue = trim($node->nodeValue);
					
					//Captura o status da transmissão (adiciona tratamento de caracteres)
					$ds_status_da_transmissao = utf8_decode($nodeValue);
					
					//Divide a descrição em palavras separadas
					$palavras = explode(" ", $ds_status_da_transmissao);
					
					//Captura a primeira palavra da descrição (verificação de status on e off)
					$primeira_palavra = $palavras[0];
					
					//Se o usuário está transmitindo
					if ($primeira_palavra == 'Transmissão')
					{
						//Registra em variável o stratus da transmissão
						$transmitindo = TRUE;
					}
					//Se o usuário não está transmitindo
					elseif ($primeira_palavra == 'Transmitido')
					{
						//Registra em variável o stratus da transmissão
						$transmitindo = FALSE;
						
						//Altera a descrição recebida pelo Youtube
						$ds_status_da_transmissao = str_replace('Transmitido ao vivo pela última vez', 'Última tramissão', $ds_status_da_transmissao);
					}
				}
			}
		}
	}
	//Se for possível carregar o arquivo HTML utilizando o método do DOMDocument
	else
	{
		//Registra que não foi possível verificar
		$ds_status_da_transmissao = "Não foi possível verificar";
	}

	//###############################################################################
	//USO DA API DO LOL PARA SABER SE O USUÁRIO ESTÁ EM JOGO
		
	//ADICIONA O INCLUDE COM A CHAVE DE ACESSO DA API DO LOL
	include ('include/riot_api_key.php');
	
	//DEFINE A URL DO SERVIDOR LOL A SER CONSULTADO
	$riot_url_base = "https://br1.api.riotgames.com";
	
	//DEFINE A FUNÇÃO DA API DO LOL A SER UTILIZADA: SUMMONERS	
	$riot_api_function = "/lol/summoner/v3/summoners/by-name/".$nm_usuario_jogo."";	
	
	//REALIZA A REQUISIÇÃO PARA CAPTURAR OS DADOS DO SERVIDOR DO LOL
	$result = file_get_contents($riot_url_base.$riot_api_function.'?api_key='.$riot_api_key.'');
	
	//CAPTURA O NOME DO INVOCADOR A PARTIR DO CÓDIGO JASON RETORNADO
	$summoner = json_decode($result);
	
	//CAPTURA OS DADOS DO INVOCADOR A PARTIR DOS DADOS RETORNADOS PELA RIOT
	$summonerId = $summoner->id;
	$summonerAccountId = $summoner->accountId;
	$summonerName = $summoner->name;
	$summonerProfileIconId = $summoner->profileIconId;
	$summonerRevisionDate = $summoner->revisionDate;
	$summonerLevel = $summoner->summonerLevel;
	
	//DEFINE A FUNÇÃO DA API DO LOL A SER UTILIZADA: ACTIVE GAMES
	$riot_api_function = "/lol/spectator/v3/active-games/by-summoner/".$summonerId."";	
	
	//VERIFICA SE O SISTEMA RETORNA INFORMAÇÕES DE QUE O USUÁRIO ESTÁ EM APRTIDA
	if($result = @file_get_contents($riot_url_base.$riot_api_function.'?api_key='.$riot_api_key.''))
	{
		//CAPTURA O NOME DO INVOCADOR A PARTIR DO CÓDIGO JASON RETORNADO
		$game = json_decode($result);
		
		//CRIA VARIÁVEL PARA ARMAZENAR O STATUS DO GAMES
		$game_status = "ONLINE";
		
		//CAPTURA OS DADOS DO INVOCADOR A PARTIR DOS DADOS RETORNADOS PELA RIOT		
		$gameID = $game->gameId;
		$gameMapId = $game->mapId;
		$gameMode = $game->gameMode;
		$gameType = $game->gameType;
		$gameStartTime = $game->gameStartTime;
		$gameLength = $game->gameLength;
	}
	//SE O JOGADOR NÃO ESTÁ EM PARTIDA
	else
	{
		//CRIA VARIÁVEL PARA ARMAZENAR O STATUS DO GAMES
		$game_status = "OFFLINE";
	}	
	
	
	//###################################################
	//CAPTURA A AÇÃO PARA CRIAÇÃO DA PARTIDA
	if (isset($_GET['acao']) && $_GET['acao'] == 'criar_partida')
	{
		//CRIA A QUERY PARA REGISTRAR NO BANCO A CRIAÇÃO DA PARTIDA
		$query_insert = "
		INSERT INTO stin_partidas 
		(
			id_streamer
		)
		VALUES
		(			
			".$id_usuario."
		)";	

		//EXECUTA A INCLUSÃO NO BANCO DE DADOS
		mysql_query($query_insert, $connect);		
	}
	
	//QUERY PARA VERIFICAR SE O STREAMER POSSUI ALGUMA PARTIDA ATIVA
	$query_select = "SELECT id_partida FROM stin_partidas WHERE id_streamer = ".$id_usuario." AND st_partida = 1";

	//EXECUTA A CONSULTA NO BANCO DE DADOS
	$select = mysql_query($query_select,$connect);
	
	//VERIFICA SE HÁ PARTIDA ATIVA
	if (mysql_num_rows($select))
	{
		//REGISTRA EM VARIÁVEL QUE HÁ PARTIDA ATIVA
		$partida_status = "ONLINE";
		
		//ARMAZENA OS DADOS DA CONSULTA EM UM ARRAY
		$array = mysql_fetch_array($select);

		//CAPTURA EM VARIÁVEIS OS DADOS DO BANCO DE DADOS
		$id_partida = $array['id_partida'];

		//FUNÇÃO PARA CONSULTAR TODAS AS SUGESTÕES DE LANE
		$select_sugestoes_lanes = F_ConsultarSugestaoDeLanes ($id_partida, $connect);
	}
	//QUANDO NÃO HÁ PARTIDA ATIVA
	else
	{
		//REGISTRA EM VARIÁVEL QUE NÃO HÁ PARTIDA ATIVA
		$partida_status = "OFFLINE";		
	}	
}
?>


<!DOCTYPE html>
<html lang="en">
<head>
	<?php include ('layout_head.php'); ?>
	<meta http-equiv="refresh" content="5" />
</head>
<body id="page-top">
	<?php include ('layout_menu.php'); ?>
    <section class="bg-primary">
        <div class="container">
			<div class="row">
				<div class="col-md-12">				
					<h1 class="section-heading text-center">PAINEL DO STREAMER</h1>
					<p class="text-center">
						Por meio desse Painel será possível utilizar as funcionalidades do sistemas de interação.<br/>					
					</p>
					<hr class="light">
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">								
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="index.php">Home</a></li>
						<li class="breadcrumb-item"><a href="streamer_inicio.php">Sreamer</a></li>
						<li class="breadcrumb-item active">Painel</li>
					</ol>
					
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title"><strong>Dados do Jogo</strong></h3>
						</div>
						<div class="panel-body">						
							<ul class="list-unstyled text-primary">
								<li><strong>Jogo:</strong> <?=$nm_jogo?></li>
								<li><strong>Usuário do Jogo:</strong> <?=$nm_usuario_jogo?></li>
								<li><strong>Status da Usuário:</strong> <?=$game_status?></li>
								<li><strong>Status da Partida:</strong> <?=$partida_status?></li>
							</ul>
						</div>
					</div>
				</div>				
				<div class="col-md-6">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title"><strong>Dados do Canal</strong></h3>
						</div>
						<div class="panel-body">						
							<ul class="list-unstyled text-primary">
								<li><strong>Canal:</strong> <?=$nm_canal?></li>
								<li><strong>Link do Canal:</strong> <a href="<?=$nm_link_canal?>" target="_blank"><?=$nm_link_canal?></a></li>
								<li><strong>Status da Transmissão:</strong> <?=$ds_status_da_transmissao?></li>
								<li>&nbsp;</li>
							</ul>
						</div>
					</div>
				</div>				
			</div>
			
			<?php
			//VERIFICA SE O STREAMER POSSUI ALGUMA PARTIDA ATIVA
			if($partida_status == "ONLINE")
			{
				?>
			
				<div class="row">
					<div class="col-md-3">
						<div class="panel panel-default">
							<div class="panel-heading text-center">							
								<h3 class="panel-title"><strong>Sugestão de Lane</strong></h3>
							</div>
							<div class="panel-body">							
								<ul class="list-unstyled text-primary">
									
									<?php								
									//APRESENTA AS SUGESTÕES DE LANE
									while ($linha_lanes = mysql_fetch_assoc($select_sugestoes_lanes))
									{										
										//APRESENTA A LANE
										echo "<li><strong>".$linha_lanes['porcentagem_principal']." ".$linha_lanes['nm_lane']."</strong></li>";										
									}
									?>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h3 class="panel-title text-center"><strong>Sugestão de Champion</strong></h3>
							</div>
							<div class="panel-body">							
								<ul class="list-unstyled text-primary">
									<li><strong>60%: Garen</strong></li>
									<li>15%: Master Yii</li>
									<li>10%: Leona</li>
									<li>5%: Lux</li>
									<li>5%: Jax</li>
									<li>1%: Zed</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h3 class="panel-title text-center"><strong>Sugestão de Build</strong></h3>
							</div>
							<div class="panel-body">
								<ul class="list-unstyled text-primary">
									<li>Anjo Guardião</li>
									<li>Glória Íntegra</li>
									<li>Lâmina de Youmuu</li>
									<li>O Cutelo Negro</li>
									<li>Couraça do Defunto</li>
									<li>Passos de Mercúrio</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h3 class="panel-title text-center"><strong>Sugestão de Target</strong></h3>
							</div>
							<div class="panel-body">
								<ul class="list-unstyled text-primary">
									<li>Multi Kill: Quadra</li>
									<li>First Blood: Sim</li>
									<li>Dano a campeões >= 10k</li>
									<li>Dano causado >= 500K</li>
									<li>Maior crítico >= 1K</li>
									<li>Sentinelas destruídas >= 5</li>
								</ul>
							</div>
						</div>
					</div>
				</div>

				<?php			
			}
			//SE O STREAMER NÃO POSSUI NENHUM PARTIDA ATIVA
			else if($partida_status == "OFFLINE")
			{
				?>
				<div class="row">
					<div class="col-md-12 text-center">
						<a href="streamer_painel.php?acao=criar_partida" class="page-scroll btn btn-default btn-xl sr-button">CRIAR PARTIDA</a>
					</div>
				</div>				
				<?php
			}
			?>
			
        </div>
    </section>
	<?php include ('layout_footer.php'); ?>
	<?php include ('layout_scripts.php'); ?>	
</body>
</html>