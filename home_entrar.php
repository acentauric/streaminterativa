<?php
//DEFINIÇÃO DO TÍTULO DA PÁGINA
$titulo_da_pagina = "Stream Interativa - Home - Entrar";

//CAPTURA ERRO SE EXISTIR
$erro = (isset($_GET["erro"]) && $_GET["erro"] != '') ? addslashes(trim($_GET["erro"])) : $erro = ""; 

//VERIFICA SE FOI ENVIADA A AÇÃO DE ADICIONAR O REGISTRO
if (isset($_GET["acao"]) && $_GET["acao"] == 'autenticar')
{
	//INCLUI AS VARIÁVEIS DE ACESSO AO BANCO DE DADOS
	include ('include/acesso_bd.php');

	//CAPTURA O EMAIL INFORMADO PELO USUÁRIO
	$email = (isset($_POST["email"]) && $_POST["email"] != '') ? addslashes(trim($_POST["email"])) : $email = ""; 

	//CAPTURA E CRIPTOGRAFA A SENHA INFORMADA PELO USUÁRIO
	$senha = (isset($_POST["senha"]) && $_POST["senha"] != '')  ? md5(trim($_POST["senha"])) : $senha = ""; 

	//VERIFICA SE O USUÁRIO NÃO FORNECEU A SENHA OU NÃO FORNECEU O EMAIL
	if(!$email || !$senha) 
	{
		//RETORNA PARA A TELA DE ERRO COM O CÓDIGO RELATIVO
		$erro = "dados_faltando";
	} 
	//SE O USUÁRIO FORNECEU A SENHA E FORNECEU O EMAIL
	else
	{
		//QUERY DE CONSULTA SE A SENHA E O EMAIL SÃO VALIDOS PARA UM USUÁRIO
		$query_select = "
		SELECT id_usuario, email FROM stin_usuarios WHERE email = '".$email."' AND senha = '".$senha."'";
		
		//EXECUTA A QUERY NO BANCO DE DADOS
		$select = mysql_query($query_select,$connect);
			
		//VERIFICA SE A QUERY NÃO RETORNOU UM USUÁRIO VÁLIDO
		if (!mysql_num_rows($select))
		{
			//RETORNA PARA A TELA DE ERRO COM O CÓDIGO 2
			$erro = "dados_invalidos";
		}
		//SE A QUERY  RETORNOU UM USUÁRIO VÁLIDO
		else
		{
			//ARMAZENA OS DADOS DE USUÁRIO RETORNADOS DO BANCO DE DADOS		
			$usuario = @mysql_fetch_array($select);
			
			//INICIA SESSÕES NO SISTEMA
			session_start();
			
			//CRIA SESSÕES DE AUTENTICAÇÃO DO USUÁRIO
			$_SESSION["id_usuario"]		= $usuario["id_usuario"]; 
			$_SESSION["email"] 			= $usuario["email"]; 
			
			//QUERY DE CAPTURA DOS TIPOS DO USUÁRIO
			$query_select = "SELECT id_tipo_usuario FROM stin_tr_usuarios_tipos_usuarios WHERE id_usuario = ".$usuario["id_usuario"]."";		
			
			//EXECUTA A CONSULTA NO BANCO DE DADOS
			$select_tr_usuarios_tipos_usuarios = mysql_query($query_select, $connect);
			
			//ARMAZENA OS TIPOS DO USUÁRIO EM SESSÃO
			while ($stin_tr_usuarios_tipos_usuarios = mysql_fetch_assoc($select_tr_usuarios_tipos_usuarios))
			{
				//ARMAZENA O TIPO DE USUÁRIO EM FORMA DE VETOR
				$_SESSION["id_tipo_usuario"][] = $stin_tr_usuarios_tipos_usuarios["id_tipo_usuario"]; 
			}
			
			//VERIFICA SE O USUÁRIO É DO TIPO STREAMER
			if (in_array("1", $_SESSION["id_tipo_usuario"]))
			{
				//ENVIA PARA A TELA DE INICIO DE UTILIZACAO
				header('Location: streamer_inicio.php');
			}
			//VERIFICA SE O USUÁRIO É DO TIPO VIEWER
			elseif (in_array("2", $_SESSION["id_tipo_usuario"]))
			{
				//ENVIA PARA A TELA DE INICIO DE UTILIZACAO
				header('Location: viewer_inicio.php');
			}
		}
	}
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include ('layout_head.php'); ?>
</head>
<body id="page-top">
	<?php include ('layout_menu.php'); ?>
    <section class="bg-primary">
        <div class="container">
			<div class="row">
				<div class="col-lg-12">
					<h1 class="section-heading text-center">ACESSE A NOSSA PLATAFORMA</h1>
					<p class="text-center">
						Para acessar a plataforma é necessário ter cadastrado usuário e senha previamente.<br/>
						Caso você ainda não tenha os dados para realizar o login, acesse o Menu superior na opção Registrar.
					</p>
					<hr class="light">
					
					<?php
					//VERIFICA SE HOUVE ERRO: FALTOU INFORMAR EMAIL OU SENHA
					if ($erro == "dados_faltando") 
					{
					?>
						<div class="alert alert-danger">
							<strong>Atenção!</strong> Informe o Login e a Senha para acessar o sistema.
						</div>	
					<?php
					}
					//VERIFICA SE HOUVE ERRO: DADOS INVÁLIDOS
					else if ($erro == "dados_invalidos") 
					{
					?>
						<div class="alert alert-danger">
							<strong>Atenção!</strong> Os dados de Email e Senha não são válidos para para acessar o sistema.
						</div>	
					<?php
					}
					//VERIFICA SE HOUVE ERRO: DADOS INVÁLIDOS
					else if ($erro == "autenticacao") 
					{
					?>
						<div class="alert alert-danger">
							<strong>Atenção!</strong> É necessário estar autenticado para acessar as funcionalidades do sistema.
						</div>	
					<?php
					}
					?>
				</div>
				<div class="col-lg-12">
					<form action="home_entrar.php?acao=autenticar" method="post">
						<div class="form-group">
							<label for="email">Email</label>
							<input type="email" class="form-control" id="email" name="email" placeholder="Email" required>
						</div>
						<div class="form-group">
							<label for="senha">Senha</label>
							<input type="password" class="form-control" id="senha" name="senha" placeholder="Senha" required>
						</div>					
						<button type="submit" class="btn btn-default">Entrar</button>
					</form>
				</div>
			</div>			
        </div>
    </section>
    <?php include ('layout_footer.php'); ?>
	<?php include ('layout_scripts.php'); ?>	
</body>
</html>