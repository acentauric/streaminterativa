<?php
//DEFINIÇÃO DO TÍTULO DA PÁGINA
$titulo_da_pagina = "Stream Interativa - Streamer - Perfil";

//INICIA SESSÕES NO SISTEMA
session_start();

//VERIFICA SE O USUÁRIO JÁ ESTÁ AUTENTICADO
if (!isset($_SESSION["id_usuario"]))
{
	//RETORNA PARA A TELA DE ERRO COM O CÓDIGO DO ERRO
	header('Location: home_entrar.php?erro=autenticacao');
}
//SE O USUÁRIO JÁ ESTÁ AUTENTICADO
else
{
	//VERIFICA SE FOI PASSADA ALGUMA MENSAGEM PARA A PÁGINA
	$mensagem = (isset($_GET["mensagem"])) ? $_GET["mensagem"] : "";
	
	//INCLUI AS VARIÁVEIS DE ACESSO AO BANCO DE DADOS
	include ('include/acesso_bd.php');
	
	//CAPTURA O ID DO USUÁRIO NAS SESSÕES DO SISTEMA
	$id_usuario = $_SESSION["id_usuario"];

	//QUERY PARA CONSULTAR OS DADOS CADASTRADOS DO USUÁRIO
	$query_select = "
	SELECT 
		email, nm_jogo, nm_usuario_jogo, nm_canal, nm_link_canal 
	FROM 
		stin_usuarios U
		LEFT JOIN stin_jogos J ON (U.id_usuario = J.id_usuario) 
		LEFT JOIN stin_canais C ON (U.id_usuario = C.id_usuario) 
	WHERE 
		U.id_usuario = '".$id_usuario."'";

	//EXECUTA A CONSULTA NO BANCO DE DADOS
	$select = mysql_query($query_select,$connect);

	//ARMAZENA OS DADOS DA CONSULTA EM UM ARRAY
	$array = mysql_fetch_array($select);

	//CAPTURA EM VARIÁVEIS OS DADOS DO BANCO DE DADOS
	$email 				= $array['email'];
	$nm_jogo 			= $array['nm_jogo'];
	$nm_usuario_jogo 	= $array['nm_usuario_jogo'];
	$nm_canal 			= $array['nm_canal'];
	$nm_link_canal 		= $array['nm_link_canal'];
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include ('layout_head.php'); ?>
</head>
<body id="page-top">
	<?php include ('layout_menu.php'); ?>
    <section class="bg-primary">
        <div class="container">			
			<h1 class="section-heading text-center">DADOS DO USUÁRIO</h1>
			<p class="text-center visible-desktop">
				As informações a seguir foram coletadas a partir do identificador do usuário.<br/>
				Confira os dados apresentados e entre em contato caso haja alguma dúvida.
			</p>
			<hr class="light">
			<div class="row">
				<div class="col-md-12">				
				
					<?php
					//VERIFICA SE HOUVE MENSAGEM
					if ($mensagem == "dados_atualizados") 
					{
					?>
						<div class="alert alert-success">
							<strong>Parabéns!</strong> Dados atualizados com sucesso!
						</div>	
					<?php
					}
					?>
				
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="index.php">Home</a></li>
						<li class="breadcrumb-item"><a href="streamer_inicio.php">Sreamer</a></li>
						<li class="breadcrumb-item active">Perfil</li>
					</ol>
					
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">				
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title"><strong>Dados Básicos</strong></h3>
						</div>
						<div class="panel-body">						
							<ul class="list-unstyled text-primary">
								<li><strong>Email:</strong> <?=$email?></li>
							</ul>
						</div>
					</div>	
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title"><strong>Dados do Jogo</strong></h3>
						</div>
						<div class="panel-body">						
							<ul class="list-unstyled text-primary">
								<li><strong>Jogo:</strong> <?=$nm_jogo?></li>
								<li><strong>Usuário do Jogo:</strong> <?=$nm_usuario_jogo?></li>                        
							</ul>
						</div>
					</div>
				</div>	
				<div class="col-md-6">				
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title"><strong>Dados do Canal</strong></h3>
						</div>
						<div class="panel-body">						
							<ul class="list-unstyled text-primary">
								<li><strong>Canal:</strong> <?=$nm_canal?></li>
								<li><strong>Link do Canal:</strong> <a href="<?=$nm_link_canal?>" target="_blank"><?=$nm_link_canal?></a></li>                        
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="text-right">
						<a href="streamer_perfil_editar.php" class="page-scroll btn btn-default btn-xl sr-button">EDITAR</a>
						<a href="streamer_painel.php" class="btn btn-default btn-xl sr-button">PAINEL</a>
					</div>
				</div>				
			</div>				
        </div>
    </section>
	<?php include ('layout_footer.php'); ?>
	<?php include ('layout_scripts.php'); ?>	
</body>
</html>