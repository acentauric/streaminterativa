<?php

//FUNÇÃO PARA CONSULTAR TODAS AS SUGESTÕES DE LANE
function F_ConsultarSugestaoDeLanes ($id_partida, $connect)
{
	//###################################################
	//QUERY PARA CONSULTAR TODAS AS SUGESTÕES DE LANE
	$query_select = "
	SELECT 
		L1.id_lane AS id_lane_principal, 
		L1.nm_lane, 
		CONCAT(ROUND(CASE WHEN L2.porcentagem_principal IS NULL THEN 0 ELSE L2.porcentagem_principal END),'%') AS porcentagem_principal
	FROM 
		stin_lanes L1
		LEFT OUTER JOIN 
		(
			SELECT 
				id_lane_principal, count(id_lane_principal), (count(id_lane_principal)/(select count(1) FROM `stin_sugestoes_lanes`  WHERE id_partida = ".$id_partida.") *100) AS porcentagem_principal			
			FROM 
				stin_sugestoes_lanes
			WHERE 
				id_partida = ".$id_partida."
			GROUP BY 
				id_lane_principal
		) L2
		ON L1.id_lane = L2.id_lane_principal
	GROUP BY 
		L1.id_lane
	ORDER BY
		L2.porcentagem_principal DESC";

	//EXECUTA A CONSULTA NO BANCO DE DADOS
	$select_sugestoes_lanes = mysql_query($query_select, $connect);
	
	//RETORNA O PONTEIRO PARA A CONSULTA REALIZADA
	return $select_sugestoes_lanes;
}
?>